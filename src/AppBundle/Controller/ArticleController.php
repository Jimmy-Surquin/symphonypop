<?php
namespace AppBundle\Controller;
use AppBundle\Entity\Article;
use AppBundle\Form\ArticleType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
/**
* @Route("/article")
*/
class ArticleController extends Controller
{
    /**
     * @Route("/new", name="article_new")
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm(ArticleType::class, new Article());
			if ($form->handleRequest($request)->isValid()) {
					$manager = $this->getDoctrine()->getEntityManagerForClass(Article::class);
					$manager->persist($form->getData());
					$manager->flush();
				$this->addFlash('success', 'Article succesfully saved');
				}
        return $this->render(':article:new.html.twig', [
				'form' => $form->createView(),
			]);
    }

	private function temp()
	{
  	return 'coucou';
	}
}
