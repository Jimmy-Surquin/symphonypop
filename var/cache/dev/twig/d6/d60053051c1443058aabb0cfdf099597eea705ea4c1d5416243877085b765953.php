<?php

/* :article:new.html.twig */
class __TwigTemplate_728b17084669d6a6e73e6b135ff7479ed5655cbe009159f210ae02211849e97f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", ":article:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a1895af2b00dc409d676534fe6a95003e717529865244ed152dc3f4feb4fd9ae = $this->env->getExtension("native_profiler");
        $__internal_a1895af2b00dc409d676534fe6a95003e717529865244ed152dc3f4feb4fd9ae->enter($__internal_a1895af2b00dc409d676534fe6a95003e717529865244ed152dc3f4feb4fd9ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":article:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a1895af2b00dc409d676534fe6a95003e717529865244ed152dc3f4feb4fd9ae->leave($__internal_a1895af2b00dc409d676534fe6a95003e717529865244ed152dc3f4feb4fd9ae_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_7c0660cd573209a159c21e697230ca9ee2a268908a896e617384579f7da146b9 = $this->env->getExtension("native_profiler");
        $__internal_7c0660cd573209a159c21e697230ca9ee2a268908a896e617384579f7da146b9->enter($__internal_7c0660cd573209a159c21e697230ca9ee2a268908a896e617384579f7da146b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "\t";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
";
        
        $__internal_7c0660cd573209a159c21e697230ca9ee2a268908a896e617384579f7da146b9->leave($__internal_7c0660cd573209a159c21e697230ca9ee2a268908a896e617384579f7da146b9_prof);

    }

    public function getTemplateName()
    {
        return ":article:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body %}*/
/* 	{{ form(form) }}*/
/* {% endblock %}*/
/* */
