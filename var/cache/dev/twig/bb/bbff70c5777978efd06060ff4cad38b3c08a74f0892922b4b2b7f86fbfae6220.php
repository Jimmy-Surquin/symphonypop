<?php

/* ::base.html.twig */
class __TwigTemplate_cfe043c574c02f20360f800deb22fb9c13dfcaa2f2636f30b5bc864bfc4c6253 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28d407acf42779915a41a60454a05de4477bbdfa6caa7a4dc3034717c516a346 = $this->env->getExtension("native_profiler");
        $__internal_28d407acf42779915a41a60454a05de4477bbdfa6caa7a4dc3034717c516a346->enter($__internal_28d407acf42779915a41a60454a05de4477bbdfa6caa7a4dc3034717c516a346_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 11
            echo "        <div class=\"flash-success\">
\t\t        ";
            // line 12
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "
        </div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "        ";
        $this->displayBlock('body', $context, $blocks);
        // line 16
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 17
        echo "    </body>
</html>
";
        
        $__internal_28d407acf42779915a41a60454a05de4477bbdfa6caa7a4dc3034717c516a346->leave($__internal_28d407acf42779915a41a60454a05de4477bbdfa6caa7a4dc3034717c516a346_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_10874679b2501e5eff0132b440814ece7ee6fb595d4e1e644d4ffead0f19c70c = $this->env->getExtension("native_profiler");
        $__internal_10874679b2501e5eff0132b440814ece7ee6fb595d4e1e644d4ffead0f19c70c->enter($__internal_10874679b2501e5eff0132b440814ece7ee6fb595d4e1e644d4ffead0f19c70c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_10874679b2501e5eff0132b440814ece7ee6fb595d4e1e644d4ffead0f19c70c->leave($__internal_10874679b2501e5eff0132b440814ece7ee6fb595d4e1e644d4ffead0f19c70c_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f4057910ca45637f5976f1970e39df0d3118aed7c610eab3134f6afbb7c0b9a9 = $this->env->getExtension("native_profiler");
        $__internal_f4057910ca45637f5976f1970e39df0d3118aed7c610eab3134f6afbb7c0b9a9->enter($__internal_f4057910ca45637f5976f1970e39df0d3118aed7c610eab3134f6afbb7c0b9a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_f4057910ca45637f5976f1970e39df0d3118aed7c610eab3134f6afbb7c0b9a9->leave($__internal_f4057910ca45637f5976f1970e39df0d3118aed7c610eab3134f6afbb7c0b9a9_prof);

    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
        $__internal_d76f7f1bfaf7cd9a5d98ef36d91b90e16c7d38bd620fe934faa9099ec72ee15e = $this->env->getExtension("native_profiler");
        $__internal_d76f7f1bfaf7cd9a5d98ef36d91b90e16c7d38bd620fe934faa9099ec72ee15e->enter($__internal_d76f7f1bfaf7cd9a5d98ef36d91b90e16c7d38bd620fe934faa9099ec72ee15e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_d76f7f1bfaf7cd9a5d98ef36d91b90e16c7d38bd620fe934faa9099ec72ee15e->leave($__internal_d76f7f1bfaf7cd9a5d98ef36d91b90e16c7d38bd620fe934faa9099ec72ee15e_prof);

    }

    // line 16
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_853f5c8003e7c0f02a38cffd61d60d62a8a80d880db152e94116db4626723dec = $this->env->getExtension("native_profiler");
        $__internal_853f5c8003e7c0f02a38cffd61d60d62a8a80d880db152e94116db4626723dec->enter($__internal_853f5c8003e7c0f02a38cffd61d60d62a8a80d880db152e94116db4626723dec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_853f5c8003e7c0f02a38cffd61d60d62a8a80d880db152e94116db4626723dec->leave($__internal_853f5c8003e7c0f02a38cffd61d60d62a8a80d880db152e94116db4626723dec_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 16,  99 => 15,  88 => 6,  76 => 5,  67 => 17,  64 => 16,  61 => 15,  52 => 12,  49 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% for flash_message in app.session.flashBag.get('success') %}*/
/*         <div class="flash-success">*/
/* 		        {{ flash_message }}*/
/*         </div>*/
/* 				{% endfor %}*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
