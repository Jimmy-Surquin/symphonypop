<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_23a7ef8a6700aa4a72e444d5efbf7440acbdc39875c0a06db066bd373c3246d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64f8184f293198486e1b060c397cc82ac95d429bda9b4df4a2405b5f066f83ab = $this->env->getExtension("native_profiler");
        $__internal_64f8184f293198486e1b060c397cc82ac95d429bda9b4df4a2405b5f066f83ab->enter($__internal_64f8184f293198486e1b060c397cc82ac95d429bda9b4df4a2405b5f066f83ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_64f8184f293198486e1b060c397cc82ac95d429bda9b4df4a2405b5f066f83ab->leave($__internal_64f8184f293198486e1b060c397cc82ac95d429bda9b4df4a2405b5f066f83ab_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_b749069613246f2654c55ba5c609a76f6715d00ffed0871a57fdfb34c6fd51fe = $this->env->getExtension("native_profiler");
        $__internal_b749069613246f2654c55ba5c609a76f6715d00ffed0871a57fdfb34c6fd51fe->enter($__internal_b749069613246f2654c55ba5c609a76f6715d00ffed0871a57fdfb34c6fd51fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_b749069613246f2654c55ba5c609a76f6715d00ffed0871a57fdfb34c6fd51fe->leave($__internal_b749069613246f2654c55ba5c609a76f6715d00ffed0871a57fdfb34c6fd51fe_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_2569479a8476432de65e95c7deb7d4f93a10affa9ec3bc534a0f9ec198c10a44 = $this->env->getExtension("native_profiler");
        $__internal_2569479a8476432de65e95c7deb7d4f93a10affa9ec3bc534a0f9ec198c10a44->enter($__internal_2569479a8476432de65e95c7deb7d4f93a10affa9ec3bc534a0f9ec198c10a44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_2569479a8476432de65e95c7deb7d4f93a10affa9ec3bc534a0f9ec198c10a44->leave($__internal_2569479a8476432de65e95c7deb7d4f93a10affa9ec3bc534a0f9ec198c10a44_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_9c94052b19b3a2732117aed82fe6c3079085c7ab80b5cd368ab64bbbda7fedc4 = $this->env->getExtension("native_profiler");
        $__internal_9c94052b19b3a2732117aed82fe6c3079085c7ab80b5cd368ab64bbbda7fedc4->enter($__internal_9c94052b19b3a2732117aed82fe6c3079085c7ab80b5cd368ab64bbbda7fedc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_9c94052b19b3a2732117aed82fe6c3079085c7ab80b5cd368ab64bbbda7fedc4->leave($__internal_9c94052b19b3a2732117aed82fe6c3079085c7ab80b5cd368ab64bbbda7fedc4_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
