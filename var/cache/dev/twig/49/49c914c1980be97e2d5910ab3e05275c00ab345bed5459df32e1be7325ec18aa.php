<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_86a4aa51e211492de670e557376333389b514e6642a8f6b239d1477e38fa4261 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_df5e1f770710c577c5404cbc76c453f571add7c19b6d1eee0e3b55c937a81923 = $this->env->getExtension("native_profiler");
        $__internal_df5e1f770710c577c5404cbc76c453f571add7c19b6d1eee0e3b55c937a81923->enter($__internal_df5e1f770710c577c5404cbc76c453f571add7c19b6d1eee0e3b55c937a81923_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_df5e1f770710c577c5404cbc76c453f571add7c19b6d1eee0e3b55c937a81923->leave($__internal_df5e1f770710c577c5404cbc76c453f571add7c19b6d1eee0e3b55c937a81923_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_2f2f9c1e201e383bf832b92d426a38a6c6fd80645a840cb199351834865497a0 = $this->env->getExtension("native_profiler");
        $__internal_2f2f9c1e201e383bf832b92d426a38a6c6fd80645a840cb199351834865497a0->enter($__internal_2f2f9c1e201e383bf832b92d426a38a6c6fd80645a840cb199351834865497a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_2f2f9c1e201e383bf832b92d426a38a6c6fd80645a840cb199351834865497a0->leave($__internal_2f2f9c1e201e383bf832b92d426a38a6c6fd80645a840cb199351834865497a0_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_3a1a9cf869fbfe05711c672b7ac27f799a9ff9b7a098d8b9e5fb3f29602d3a87 = $this->env->getExtension("native_profiler");
        $__internal_3a1a9cf869fbfe05711c672b7ac27f799a9ff9b7a098d8b9e5fb3f29602d3a87->enter($__internal_3a1a9cf869fbfe05711c672b7ac27f799a9ff9b7a098d8b9e5fb3f29602d3a87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_3a1a9cf869fbfe05711c672b7ac27f799a9ff9b7a098d8b9e5fb3f29602d3a87->leave($__internal_3a1a9cf869fbfe05711c672b7ac27f799a9ff9b7a098d8b9e5fb3f29602d3a87_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_a0f3168efb742d164bc8ae98ed174c4a41836ee9286508876ff98e2718aea432 = $this->env->getExtension("native_profiler");
        $__internal_a0f3168efb742d164bc8ae98ed174c4a41836ee9286508876ff98e2718aea432->enter($__internal_a0f3168efb742d164bc8ae98ed174c4a41836ee9286508876ff98e2718aea432_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_a0f3168efb742d164bc8ae98ed174c4a41836ee9286508876ff98e2718aea432->leave($__internal_a0f3168efb742d164bc8ae98ed174c4a41836ee9286508876ff98e2718aea432_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
